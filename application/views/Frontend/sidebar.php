

						<div class="section">

							<h2>Latest News &amp; Media</h2>
							<div class="news_articles">
								<div class="item">
									<a
										href="news/2016/new-hope-welcomes-start-of-land-court-hearings.html">
										<img src="img/news_noimage.png" alt="" />
										<div class="description">
											<span class="publish_date">(07/03/16)</span><br> New Hope
											welcomes start of Land Court hearings
											<div class="readmore">Read More &gt;</div>
										</div>
									</a>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="news_articles">
								<div class="item">
									<a href="news/2016/new-hope-bengalla-acquisition-update.html">
										<img src="img/news_noimage.png" alt="" />
										<div class="description">
											<span class="publish_date">(08/02/16)</span><br> New Hope
											Bengalla Acquisition Update
											<div class="readmore">Read More &gt;</div>
										</div>
									</a>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="news_articles">
								<div class="item">
									<a
										href="news/2016/new-hope-hones-emergency-response-skills.html">
										<img
										src="generated/images/nhg-employee-and-paramedic_newsthumb.jpg"
										alt="" />
										<div class="description">
											<span class="publish_date">(13/01/16)</span><br> New Hope
											hones emergency response skills
											<div class="readmore">Read More &gt;</div>
										</div>
									</a>
									<div class="clearfix"></div>
								</div>
							</div>

							<a href="news.html" class="action_button">More News</a>
						</div>

						<div class="section">

							<h2>Related Links</h2>

							<ul class="links">
								<li><a
									href="files/files/The%20Whole%20Story_Mining%27s%20contribution%20to%20the%20Australian%20community_FINAL.pdf">
										<div class="description">
											<span class="publish_date">(12/11/15)</span><br> The Whole
											Story &mdash; Mining's contribution to the Australian
											community &gt;
										</div>
								</a></li>
								<li><a
									href="files/files/150625%20Coal%20-%20The%20Hard%20Facts%20-%20information%20sheet.pdf">
										<div class="description">
											<span class="publish_date">(25/06/15)</span><br> Coal. The
											Hard Facts &mdash; an information sheet from the Minerals
											Council of Australia &gt;
										</div>
								</a></li>
								<li><a
									href="http://www.minerals.org.au/file_upload/files/resources/coal/Coal_Hard_Facts.pdf">
										<div class="description">
											<span class="publish_date">(12/06/14)</span><br> Coal Hard
											Facts &mdash; a booklet from the Minerals Council of
											Australia &gt;
										</div>
								</a></li>
							</ul>
						</div>