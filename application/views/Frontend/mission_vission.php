

	<div class="content content-with-pullout">
		<h1>What we believe</h1>
		<!--span class="subtitle"></span-->
		<div class="description">
			<p class="pullout">Our business objectives are supported through our
				Vision and Values</p>
			<h3>
				<strong>VISION</strong>
			</h3>
			<p class="standout">
				<strong>New Hope is a successful diversified mining and energy
					business. We are proud of our achievements and care about people
					and the environment. We will deliver sustainable growth and
					enduring shareholder value through our people and quality assets.</strong>
			</p>
			<hr>
			<h3>
				<strong>VALUES</strong>
			</h3>
			<p class="standout">
				<strong>Our core values underpin what we do and how we do it.</strong>
			</p>
			<ul>
				<li style="margin-left: 40px;"><strong>INTEGRITY. </strong>We are <em>ethical,
						honest</em> and can be&nbsp;<em>trusted&nbsp;</em>to do the right
					thing.</li>
			</ul>
			<ul>
				<li style="margin-left: 40px;"><strong>RESPECT.</strong> We <em>listen</em>
					to our stakeholders and treat others as we expect to be treated
					ourselves.</li>
			</ul>
			<ul>
				<li style="margin-left: 40px;"><strong>ACCOUNTABILITY. </strong>We <em>act</em>
					in&nbsp;accordance with our obligations, <em>deliver</em> on our
					commitments and <em>take responsibility</em> for our actions.</li>
			</ul>
			<ul>
				<li style="margin-left: 40px;"><strong>SAFETY.&nbsp;</strong>We
					share a mutual responsibility to&nbsp;<em>prevent harm </em>and&nbsp;<em>promote
						wellbeing.</em></li>
			</ul>
			<ul>
				<li style="margin-left: 40px;"><strong>RESILIENCE.</strong> We
					strive to&nbsp;<em>achieve long term sustainability&nbsp;</em>by
					navigating through change and uncertainty.</li>
			</ul>
			<ul>
				<li style="margin-left: 40px;"><strong>SUCCESS. </strong>We take
					pride in the&nbsp;<em>achievement of our goals</em>, being&nbsp;<em>innovative</em>
					and&nbsp;<em>making a positive difference.</em></li>
			</ul>
		</div>
	</div>

