<div id="hd">
				<a href="<?php echo base_url();?>" id="logo">NAIL</a>
				<ul id="nav">
					<li id="navabout"><a href="<?php echo base_url()?>frontend/about"><span>About Us</span></a>
					<ul>
							<li id="navaboutabout-the-new-hope-group"><a
								href="<?php echo base_url(); ?>frontend/about"><span>About NAIL</span></a></li>
							<li id="navaboutleadership"><a
								href="<?php echo base_url()?>frontend/management"><span>Management Team</span></a>
							<ul class="subnav">
									<li id="navaboutleadershipshane-stephan"><a
										href="content/about/leadership/shane-stephan.html"><span>Shane
												Stephan</span></a></li>
									<li id="navaboutleadershipandrew-boyd"><a
										href="content/about/leadership/andrew-boyd.html"><span>Andrew
												Boyd</span></a></li>
									<li id="navaboutleadershipmatthew-busch"><a
										href="content/about/leadership/matthew-busch.html"><span>Matthew
												Busch</span></a></li>
									<li id="navaboutleadershipjustin-hogg"><a
										href="content/about/leadership/justin-hogg.html"><span>Justin
												Hogg</span></a></li>
									<li id="navaboutleadershipjim-randell"><a
										href="content/about/leadership/jim-randell.html"><span>Jim
												Randell</span></a></li>
									<li id="navaboutleadershipkim-franks"><a
										href="content/about/leadership/kim-franks.html"><span>Kim
												Franks</span></a></li>
									<li id="navaboutleadershipstephen-eames"><a
										href="content/about/leadership/stephen-eames.html"><span>Stephen
												Eames</span></a></li>
									<li id="navaboutleadershipsam-fisher"><a
										href="content/about/leadership/sam-fisher.html"><span>Sam
												Fisher</span></a></li>
								</ul></li>
							<li id="navaboutvision-and-values"><a
								href="<?php echo base_url();?>frontend/MissionVission"><span>Mission & Vission</span></a></li>
							<li id="navaboutenvironment"><a
								href="<?php echo base_url()?>frontend/environment"><span>Environment</span></a></li>
							
							<li id="navaboutour-history"><a
								href="<?php echo base_url()?>frontend/history"><span>Our History</span></a></li>
						</ul></li>
						
					<li id="navprojects"><a href="content/projects.html"><span>Products</span></a>
					<ul>
							<li id="navprojectsoperations"><a
								href="content/projects/operations/coal/index.html"><span>Soybean</span></a>
							<ul>
									<li id="navprojectsoperationscoal"><a
										href="content/projects/operations/coal.html"><span>Soybean Seed</span></a></li>
									<li id="navprojectsoperationsnew-acland-1"><a
										href="content/projects/operations/new-acland-1.html"><span>Full Fat Soybean</span></a></li>
									<li id="navprojectsoperationswest-moreton"><a
										href="content/projects/operations/west-moreton.html"><span>Soybean Meal/Cake</span></a></li>
									<li id="navprojectsoperationsagriculture"><a
										href="content/projects/operations/agriculture.html"><span>Soybean Oil</span></a></li>
									<li id="navprojectsoperationsoil-1"><a
										href="content/projects/operations/oil-1.html"><span>Soybean Extraction</span></a></li>
									
								</ul></li>
							<li id="navprojectsdevelopment"><a
								href="content/projects/development/new-acland/index.html"><span>Meal</span></a>
							<ul>
									<li id="navprojectsdevelopmentnew-acland"><a
										href="http://www.aclandproject.com.au/"><span>Fish Meal</span></a></li>
									<li id="navprojectsdevelopmentcolton"><a
										href="content/projects/development/colton.html"><span>Poultry Meal</span></a></li>
									<li id="navprojectsdevelopmentlenton"><a
										href="content/projects/development/lenton.html"><span>Guar Meal</span></a></li>
									<li id="navprojectsdevelopmentelimatta"><a
										href="content/projects/development/elimatta.html"><span>DDGS</span></a></li>
									<li id="navprojectsdevelopmentYamala"><a
										href="content/projects/development/Yamala.html"><span>Meat & Bone Meal</span></a></li>
									
								</ul></li>
							<li id="navprojectsinnovation"><a
								href="content/projects/innovation/coal-to-liquids/index.html"><span>Other Products</span></a>
							<ul>
									<li id="navprojectsinnovationcoal-to-liquids"><a
										href="content/projects/innovation/coal-to-liquids.html"><span>Maize</span></a></li>
									<li id="navprojectsinnovationcoal-to-liquids"><a
										href="content/projects/innovation/coal-to-liquids.html"><span>Wheat</span></a></li>
									<li id="navprojectsinnovationcoal-to-liquids"><a
										href="content/projects/innovation/coal-to-liquids.html"><span>Rapeseed</span></a></li>
									<li id="navprojectsinnovationcoal-to-liquids"><a
										href="content/projects/innovation/coal-to-liquids.html"><span>Limestone</span></a></li>
									<li id="navprojectsinnovationcoal-to-liquids"><a
										href="content/projects/innovation/coal-to-liquids.html"><span>Pum Oil</span></a></li>
								</ul></li>
						</ul></li>
					<li id="navcommunity"><a href="content/community.html"><span>Community</span></a>
					<ul>
							<li id="navcommunityoverview"><a href="content/community.html"><span>Overview</span></a></li>
							<li id="navcommunitysponsorship-donation"><a
								href="content/community/sponsorship-donation.html"><span>Sponsorship
										&amp; Donation Program</span></a></li>
							<li id="navcommunitymemberships"><a
								href="content/community/memberships.html"><span>Community
										Participation</span></a></li>
							<li id="navcommunitycareflight"><a
								href="content/community/careflight.html"><span>Careflight</span></a></li>
							<li id="navcommunitycontact-1"><a
								href="content/community/contact-1.html"><span>Contact Us</span></a></li>
						</ul></li>
					<li id="navcareers"><a href="content/careers.html"><span>Careers</span></a>
					<ul>
							<li id="navcareersworking-with-new-hope"><a
								href="content/careers.html"><span>Working with New Hope</span></a></li>
							<li id="navcareerscurrent-opportunities"><a
								href="content/careers/current-opportunities.html"><span>Current
										opportunities</span></a></li>
							<li id="navcareersour-people"><a
								href="content/careers/our-people.html"><span>Our People</span></a></li>
							<li id="navcareersour-culture"><a
								href="content/careers/our-culture.html"><span>Our Culture</span></a></li>
						</ul></li>
					<li id="navnews"><a href="news/index.html"><span>News</span></a>
					<ul>
							<li id="navnewsmedia-releases"><a href="news.html"><span>News and
										Media Releases</span></a></li>
							<li id="navnewsmultimedia"><a href="content/news/multimedia.html"><span>Multimedia</span></a></li>
							<li id="navnewscommunity-newsletters"><a
								href="content/news/community-newsletters.html"><span>Community
										Newsletters</span></a></li>
							<li id="navnewscontact-us-1"><a
								href="content/news/contact-us-1.html"><span>Media Enquiries</span></a></li>
						</ul></li>
					<li id="navinvestors"><a href="content/investors.html"><span>Investors</span></a>
					<ul>
							<li id="navinvestorsinvestor-information"><a
								href="content/investors.html"><span>Investor Information</span></a></li>
							<li id="navinvestorsasx-announcements"><a
								href="content/investors/asx-announcements.html"><span>ASX
										Announcements</span></a>
							<ul>
									<li id="navinvestorsasx-announcements2014-asx-announcements"><a
										href="content/investors/asx-announcements/2014-asx-announcements.html"><span>2014
												ASX Announcements</span></a></li>
									<li id="navinvestorsasx-announcements2013-asx-announcements"><a
										href="content/investors/asx-announcements/2013-asx-announcements.html"><span>2013
												ASX Announcements</span></a></li>
									<li id="navinvestorsasx-announcements2012-asx-announcements"><a
										href="content/investors/asx-announcements/2012-asx-announcements.html"><span>2012
												ASX Announcements</span></a></li>
									<li id="navinvestorsasx-announcements2011-asx-announcements"><a
										href="content/investors/asx-announcements/2011-asx-announcements.html"><span>2011
												ASX Announcements</span></a></li>
								</ul></li>
							<li id="navinvestorsannual-reports"><a
								href="content/investors/annual-reports.html"><span>Annual
										Reports</span></a></li>
							<li id="navinvestorsquarterly-reports"><a
								href="content/investors/quarterly-reports.html"><span>Quarterly
										Reports</span></a></li>
							<li id="navinvestorsasx-information"><a
								href="content/investors/asx-information.html"><span>Share
										Registry</span></a></li>
							<li id="navinvestorscorporate-governance"><a
								href="content/investors/corporate-governance.html"><span>Corporate
										Governance</span></a></li>
						</ul></li>
					<li id="navcontact-us"><a href="content/contact-us.html"><span>Contact
								Us</span></a></li>
				</ul>
			</div>