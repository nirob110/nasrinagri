	<div class="content content-with-pullout">
		<h1>Leadership</h1>
		<!--span class="subtitle"></span-->
		<div class="description">
		<p class="standout">
	Proactive management, strong partnerships and a people-centred culture have been the hallmarks of New Hope�s success. The leadership team drives an ambitious agenda with a dual focus on maximising productivity and minimising costs today while exploring new opportunities for tomorrow.</p>
<p class="pullout">
	Our Directors bring a <em>wealth </em>of <em>experience</em></p>
<p>
	Diversification has seen the&nbsp;business grow to ensure value for shareholders and a positive, long-term legacy for the communities in which we operate.</p>
<p>
	Our directors bring a wealth of expertise to guide New Hope�s development. Combined with extensive strategic planning, project management, governance, and finance and investment insight from the directors and leadership team, New Hope is well positioned for sustainable growth and success.</p>
<hr><h2>
	<strong>Directors</strong></h2>
<ul><li>
		Mr Robert Millner � <em>Chairman</em></li>
	<li>
		Mr Shane Stephan - <em>Managing Director</em></li>
	<li>
		Mr Bill Grant �<em> Non-Executive Director</em></li>
	<li>
		Mrs Sue Palmer � <em>Non-Executive Director</em></li>
	<li>
		Mr Ian Williams � <em>Non-Executive Director</em></li>
	<li>
		Mr Todd Barlow -&nbsp;<em>Non-Executive Director</em><em>&nbsp;</em></li>
	<li>
		Mr Tom Millner - <em>Non-Executive Director</em></li>
</ul><hr><h2>
	<strong>Executive Leadership Group</strong></h2>
<table style="height:409px;width:606px;" align="left" border="1" cellpadding="1" cellspacing="0"><tbody><tr><td>
				<a href="leadership/shane-stephan"><img alt="" src="/files/images/140702%20Shane%20Stephan%20medium%20res.jpg" style="height:140px;width:210px;"></a><span>&nbsp;</span></td>
			<td>
				<img alt="" src="/files/images/140702%20Andrew%20Boyd%20medium%20res.jpg" style="height:140px;width:210px;"></td>
		</tr><tr><td>
				<h3>
					<a href="leadership/shane-stephan">Shane Stephan</a><span>&nbsp;</span></h3>
				<h4>
					Managing Director</h4>
			</td>
			<td>
				<h3>
					<a href="http://www.newhopegroup.com.au/content/about/leadership/andrew-boyd">Andrew Boyd</a></h3>
				<h4>
					Chief Operating Officer</h4>
			</td>
		</tr><tr><td>
				&nbsp;</td>
			<td>
				&nbsp;</td>
		</tr><tr><td>
				<h3>
					<a href="leadership/matthew-busch"><img alt="" src="/files/images/140702%20Matthew%20Busch%20medium%20res.jpg" style="height:140px;width:210px;"></a></h3>
			</td>
			<td>
				<h3>
					<a href="leadership/justin-hogg"><img alt="" src="/files/images/140702%20Justin%20Hogg%20medium%20res.jpg" style="height:140px;width:210px;"></a></h3>
			</td>
		</tr><tr><td>
				<h3>
					<a href="leadership/matthew-busch">Matthew Busch</a><span>&nbsp;</span></h3>
				<h4>
					Chief Financial Officer</h4>
			</td>
			<td>
				<h3>
					<a href="leadership/justin-hogg">Justin Hogg</a></h3>
				<h4>
					Company Secretary</h4>
			</td>
		</tr><tr><td>
				&nbsp;</td>
			<td>
				&nbsp;</td>
		</tr><tr><td>
				<img alt="" src="/files/images/140702%20Jim%20Randell%20medium%20res(1).jpg" style="height:140px;width:210px;"></td>
			<td>
				&nbsp;</td>
		</tr><tr><td>
				<h3>
					<a href="leadership/jim-randell">Jim Randell</a></h3>
				<h4>
					Executive General Manager&nbsp;- Mining<span>&nbsp;</span></h4>
			</td>
			<td>
				<h3>
					<span>&nbsp;<span>&nbsp;</span></span></h3>
			</td>
		</tr><tr><td>
				&nbsp;</td>
			<td>
				&nbsp;</td>
		</tr><tr><td>
				<p>
					<img alt="" src="/files/images/Kim%20Franks%20photo%20-%20medium%20res(1).jpg" style="height:140px;width:210px;"></p>
			</td>
			<td>
				<p>
					<span><img alt="" src="/files/images/140702%20Sam%20Fisher%20medium%20res.jpg" style="height:140px;width:210px;"></span></p>
			</td>
		</tr><tr><td>
				<h3>
					<a href="leadership/kim-franks">Kim Franks</a></h3>
				<h4>
					General Manager - People</h4>
			</td>
			<td>
				<h3>
					<a href="leadership/sam-fisher">Sam Fisher</a></h3>
				<h4>
					General Manager&nbsp;- Marketing &amp; Logistics<span>s<span>&nbsp;</span></span></h4>
			</td>
		</tr><tr><td>
				&nbsp;</td>
			<td>
				&nbsp;</td>
		</tr><tr><td style="height:100px;vertical-align:top;text-align:left;">
				<h3>
					<img alt="" src="/files/images/140702%20Stephen%20Eames%20medium%20res.jpg" style="height:140px;width:210px;"></h3>
			</td>
			<td style="height:100px;vertical-align:middle;text-align:left;">
				&nbsp;</td>
		</tr><tr><td>
				<h3>
					<a href="leadership/stephen-eames">Stephen Eames</a></h3>
				<h4>
					General Manager&nbsp;- Resource Development</h4>
			</td>
			<td>
				<h3>
					&nbsp;</h3>
			</td>
		</tr><tr><td>
				&nbsp;</td>
			<td>
				&nbsp;</td>
		</tr><tr><td>
				&nbsp;</td>
			<td>
				&nbsp;</td>
		</tr></tbody></table><h2>
	&nbsp;</h2>		</div>
			</div>

