
<div class="content content-with-pullout">
	<h1>Our History</h1>
	<!--span class="subtitle"></span-->
	<div class="description">
		<p class="standout">New Hope began operating coal mines in the Ipswich
			region of Queensland in 1952.</p>
		<p>Since then, the business has seen dynamic improvement to mining
			methods, operating and safety standards, as well as dramatic changes
			to domestic and export markets.</p>
		<hr>
		<p class="pullout">1952</p>
		<ul>
			<li>New Hope commences operations in Ipswich, Queensland<br> &nbsp;
			</li>
		</ul>
		<p class="pullout">1970</p>
		<ul>
			<li>Washington H. Soul Pattinson, Farjoy Pty Ltd and Domer Mining Co.
				Pty Ltd, acquire control of the company</li>
			<li>New Hope exceeds 250,000 tonnes per annum (tpa) of marketable
				coal from underground mining<br> &nbsp;
			</li>
		</ul>
		<p class="pullout">1978</p>
		<ul>
			<li>Production increases to almost 340,000 tpa</li>
			<li>New Hope purchases the Tivoli Collieries operation including the
				barge coal facilities and operation<br> &nbsp;
			</li>
		</ul>
		<p class="pullout">1979</p>
		<ul>
			<li>New Hope purchases the Southern Cross and Haighmoor Collieries
				operations<br> &nbsp;
			</li>
		</ul>
		<p class="pullout">1980</p>
		<ul>
			<li>New Hope dispatches the first export shipment � one of the first
				companies to successfully obtain trials of Ipswich coal in the
				Japanese market</li>
		</ul>
		<p class="pullout">1981</p>
		<ul>
			<li>New Hope purchases the Rylance Collieries operations<br> &nbsp;
			</li>
		</ul>
		<p class="pullout">1982</p>
		<ul>
			<li>New Hope commences the West Moreton mining operations with the <a
				href="/content/projects/operations/coal/jeebropilly">Jeebropilly
					Mine</a><br> &nbsp;
			</li>
		</ul>
		<p class="pullout">1983</p>
		<ul>
			<li>Acquisitions and open cut mining methods see production near 1.2
				million tonnes per annum (Mtpa).</li>
			<li>Export commences through <a
				href="/content/projects/operations/port">Queensland Bulk Handling</a>&nbsp;(QBH)<br>
				&nbsp;
			</li>
		</ul>
		<p class="pullout">1985</p>
		<ul>
			<li>New Hope acquires a 40% interest in PT Multi Harapan Utama,
				commencing two decades of operations in Indonesia</li>
			<li>New Hope wins the Austrade Australian Export Award for
				Outstanding Export Achievement<br> &nbsp;
			</li>
		</ul>
		<p class="pullout">1989</p>
		<ul>
			<li>Interest is acquired in PT Adaro Indonesia with commercial coal
				produced in 1992<br> &nbsp;
			</li>
		</ul>
		<p class="pullout">1993</p>
		<ul>
			<li>New Hope wins the Queensland Premier�s Award for Environmental
				Excellence in the Queensland mining industry<br> &nbsp;
			</li>
		</ul>
		<p class="pullout">1994</p>
		<ul>
			<li>New Hope wins the&nbsp;Queensland Premier�s Awards Commendation
				in recognition of innovative rehabilitation practice<br> &nbsp;
			</li>
		</ul>
		<p class="pullout">1995</p>
		<ul>
			<li>New Hope purchases the Rhonnda Collieries operations<br> &nbsp;
			</li>
		</ul>
		<p class="pullout">1996</p>
		<ul>
			<li>New Hope�s interest in Multi Harapan is sold to concentrate on
				Adaro, while a deep-water bulk handling coal terminal facility is
				developed in Indonesia<br> &nbsp;
			</li>
		</ul>
		<p class="pullout">1999</p>
		<ul>
			<li>New Hope acquires the Acland deposit in South East Queensland</li>
			<li>The Company purchases the Oakleigh Collieries<br> &nbsp;
			</li>
		</ul>
		<p class="pullout">2002</p>
		<ul>
			<li><a href="/content/projects/operations/coal/new-acland-1">New
					Acland Coal Mine</a> is commissioned at an initial marketable coal
				production rate of 2 Mtpa</li>
			<li>The West Moreton region�s two mines (Jeebropilly and New
				Oakleigh) are awarded the �Excellence in Training in the Coal Sector
				Award� by the Queensland Mining Industry Training Advisory Body<br>
				&nbsp;
			</li>
		</ul>
		<p class="pullout">2003</p>
		<ul>
			<li>New Hope lists on the ASX</li>
			<li>The Queensland Department of Natural Resources and Mines awards
				the Mines Inspectorate �Best Safety Management in Queensland �
				Southern Region� award to New Acland</li>
			<li>New Oakleigh is awarded the Mines Inspectorate �Most Improved
				Safety Management in Queensland � Southern Region� Award<br> &nbsp;
			</li>
		</ul>
		<p class="pullout">2004</p>
		<ul>
			<li>The New Acland Coal mine�s production increases to 2.5 Mtpa<br>
				&nbsp;
			</li>
		</ul>
		<p class="pullout">2005</p>
		<ul>
			<li>New Hope sells its interests in overseas operations to
				concentrate on its Australian operations<br> &nbsp;
			</li>
		</ul>
		<p class="pullout">2006</p>
		<ul>
			<li>New Hope wins the �Premier of Queensland Export Minerals and
				Energy Award�</li>
			<li>The <a href="/content/projects/operations/agriculture">Acland
					Pastoral Company (APC)</a>&nbsp;is established
			</li>
			<li>Winner of the Queensland Japan Chamber of Commerce &amp; Industry
				Minerals &amp; Energy Category<br> &nbsp;
			</li>
		</ul>
		<p class="pullout">2007</p>
		<ul>
			<li>The company acquires the remaining 50% stake in Queensland Bulk
				Handling<br> &nbsp;
			</li>
		</ul>
		<p class="pullout">2009</p>
		<ul>
			<li>An initial 25% equity interest is acquired in an exclusive,
				worldwide, licensed <a
				href="/content/projects/innovation/coal-to-liquids">coal-to-liquids
					technology</a> with Quantex Energy Incorporated<br> &nbsp;
			</li>
		</ul>
		<p class="pullout">2010</p>
		<ul>
			<li>New Hope makes its first acquisition of <a
				href="/content/projects/interests/westside-corp">WestSide
					Corporation</a> shares<br> &nbsp;
			</li>
		</ul>
		<p class="pullout">2011</p>
		<ul>
			<li>New Hope takes ownership of Northern Energy Corporation (NEC)</li>
			<li>Acland Pastoral Company starts scientifically-controlled grazing
				trials on rehabilitated land<br> &nbsp;
			</li>
		</ul>
		<p class="pullout">2012</p>
		<ul>
			<li>New Hope acquires ownership of <a
				href="/content/projects/operations/oil-1">Bridgeport Energy</a>&nbsp;�
				an emerging Australian oil and gas production development and
				exploration company
			</li>
			<li>New Hope makes early moves into minerals exploration</li>
			<li>Total clean coal production reaches a record 6.29 Mtpa</li>
			<li>New Hope wins the �<em><a
					href="/news/2012/new-hope-wins-ipswich-business-of-the-year-award">Ipswich
						Business of the Year</a></em>� Award<br> &nbsp;
			</li>
		</ul>
		<p class="pullout">2013</p>
		<ul>
			<li><a href="/news/2013/new-oakleigh-finishes-mining-operations">New
					Oakleigh Coal Mine&nbsp;closes</a> in January with rehabilitation
				of the site ongoing</li>
			<li>Queensland Bulk Handling <a
				href="http://www.newhopegroup.com.au/news/2013/qbh-celebrates-milestone">celebrates
					30 Years of operations</a></li>
			<li>The Company&nbsp;launches the <a
				href="http://www.newhopegroup.com.au/news/2013/new-acland-delivers-on-community-investment">New
					Acland Community Investment Fund</a><br> &nbsp;
			</li>
		</ul>
	</div>
</div>

