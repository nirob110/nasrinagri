<div class="content">

						<div class="description">
							<p class="standout">
								The New Hope Group is an Australian owned and operated
								diversified<br /> energy company which has been proudly based in
								South East Queensland<br /> for more than 60 years.
							</p>
							<p>
								With business interests and operations spanning coal mining,
								exploration, port operation,<br /> oil, agriculture, innovative
								technologies and investment, New Hope is ranked as one of<br />
								the 150 largest ASX listed companies by market capitalisation,
								and is amongst the<br /> top 15 ASX listed companies based in
								Queensland. <span> </span>
							</p>
							<p>
								Our continued growth is founded on a long-term commitment to our
								employees and<br /> disciplined management, alongside a
								proactive approach to the environment, community<br /> and
								social responsibility.
							</p>
							<p> </p>
							<h4 class="standout">
								<em><strong><a href="http://www.aclandproject.com.au/">Visit the
											New Acland Website.</a></strong></em>
							</h4>
							<h4 class="standout">
								<strong><a
									href="http://www.aclandproject.com.au/content/stage-3-eis-2014"><em>View
											and download the AEIS and the EIS here.</em></a></strong>
							</h4>
							<h4 class="standout">
								<a href="files/files/NHG0040_ExecSummary_NAP_EIS_WebRes_Ex.pdf"><strong><em>View
											the New Acland Coal Mine Stage 3 Project Environmental Impact
											Statement EXECUTIVE SUMMARY here.</em></strong></a>
							</h4>
							<h4 class="standout">
								<strong><em><span> </span></em></strong><strong><em><span> </span></em></strong>
							</h4>
							<hr />
						</div>

						<div class="home-tile home-tile-first">
							<a href="content/news/multimedia.html"> <img
								src="<?php echo base_url();?>frontend/generated/images/aclandpastoralfront-hometile-1_hometile.jpg"
								alt="" />
								<div class="title">View our Information Campaign</div>
								<div class="arrow"></div>
							</a>
						</div>
						<div class="home-tile ">
							<a href="content/community.html"> <img
								src="<?php echo base_url();?>frontend/generated/images/in-the-community_hometile.jpg" alt="" />
								<div class="title">In The Community</div>
								<div class="arrow"></div>
							</a>
						</div>

						<div class="clearfix"></div>

						<div class="home-tile home-tile-large">
							<a href="#"> <img alt="">
								<div class="arrow"></div>
							</a>
							<div class="title">
								<a
									href="news/2013/district-welcomes-new-emergency-services-resources.html"
									data-image="<?php echo base_url();?>frontend/generated/images/130227-nac-jondaryan-rural-fire-brigade-028-3_campaigntile.jpg">Supporting
									the Rural Fire Brigade</a> <span>&nbsp;&nbsp; | &nbsp;&nbsp;</span>
								<a
									href="news/2012/new-hope-wins-ipswich-business-of-the-year-award.html"
									data-image="<?php echo base_url();?>frontend/generated/images/phpthumb-generated-thumbnail2_campaigntile.jpg">New
									Hope wins Business of the Year Award</a>
							</div>
						</div>


					</div>