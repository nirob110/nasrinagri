
	<div class="content content-with-pullout">
		<h1>Environment</h1>
		<!--span class="subtitle"></span-->
		<div class="description">
			<p class="standout">Effective and holistic environmental management
				is crucial to the success of our operations. We work to identify and
				use the most practical and sustainable methods of environmental
				management to minimise the impacts of our activities on the
				environment and community. This is essential not only to our
				business but to the sustainable development of Australia�s mining
				industry.</p>
			<p>An&nbsp;Environmental Management System guides New Hope�s
				environmental practices, supported by advice from independent
				environmental consultants. This assists the company to improve
				environmental performance by increasing awareness, optimising
				operational control, monitoring compliance and enabling continuous
				improvement.</p>
			<p>
				<a href="sustainability/environmental-management-system">Learn more
					about our Environmental Management System.</a>
			</p>
			<hr>
			<h3>
				<strong>PROACTIVE ENVIRONMENTAL MANAGEMENT</strong>
			</h3>
			<p>Above and beyond requirements, we continually search for
				improvements&nbsp;that will provide better outcomes for the
				environment and communities near our operations.</p>
			<p class="pullout">
				<em>Above</em> and <em>beyond</em> requirements
			</p>
			<p>In May 2013, New Hope became the first company transporting coal
				along the western rail corridor to begin the process of profiling
				and veneering coal wagons. Trains from the New Acland mine are
				coated with an environmentally-friendly veneering solution which can
				reduce the amount of dust from wagons by up to 80 percent. The
				solution dries to form a flexible 'crust' over the coal and
				physically prevents dust from being released from the wagons.</p>
			<p>We have some of the most highly sensitive and sophisticated dust
				monitoring equipment in the country operating at our rail loading
				facility near Jondaryan. Two Tapered Element Oscillating
				Microbalance (TEOM) dust monitoring systems provide accurate&nbsp;24
				hour a day monitoring.</p>
			<p>Our New Acland mine has installed electric horns on machinery and
				is trialling an upgraded state-of-the-art muffler system on trucks
				with the aim of further reducing noise.</p>
			<p>&nbsp;</p>
			<p>
				<img alt="" src="/files/images/_NSP4451.jpg"
					style="height: 333px; width: 500px;">
			</p>
			<hr>
			<h2>
				<strong>Green Initiatives</strong>
			</h2>
			<p class="pullout">
				New Hope undertakes a number of <em>environmental initiatives</em>
			</p>
			<p>The purchase of locally produced recycled water for use at New
				Acland, a native tree and seeding program with Greening Australia,
				and scientifically-controlled grazing trials monitored by leading
				independent livestock consultants and a university, are just some of
				the initiatives we are undertaking in partnership with industry and
				community enterprises as part of our environmental program.&nbsp;</p>
			<p>Among our environmental initiatives is our involvement in the
				Federal Government's Energy Efficiency Opportunities program which
				requires businesses to identify, evaluate and report publicly on
				cost effective energy saving oppportunites.</p>
			<h4>
				<a href="environment/energy-efficiency-opportunities">Learn more
					about our Energy Efficiency Opportunities (EEO)</a>
			</h4>
			<hr>
			<h3>
				<strong>AIR QUALITY MANAGEMENT</strong>
			</h3>
			<p>New Hope continuously monitors the air quality conditions at its
				rail loading facilities. It operates according to strict
				environmental requirements of the Environmental Protection Act 1994
				and specifically meets all the Environmental Authority (EA) and
				Development Approval (DA) conditions set by the Queensland
				Government.</p>
			<p>New Hope goes beyond requirements in the best interests of its
				neighbours, communities and the wider region.</p>
			<p>In May&nbsp;2013, the New Hope group became the first company
				transporting coal along the South West System to begin the process
				of profiling and veneering coal wagons.</p>
			<h4>
				<a target="_blank"
					href="https://www.qrc.org.au/01_cms/details.asp?ID=3366">Click here
					for more information regarding&nbsp;Air Quality Management</a>
			</h4>
			<h4>
				<a class="lightbox" target="_blank"
					href="http://www.youtube.com/watch?v=Ud34bQr6WSo">Learn more
					about&nbsp;Coal Dust</a>
			</h4>
			<h4>&nbsp;</h4>
			<h4>&nbsp;</h4>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
		</div>
	</div>
