
<?php include_once 'header.php';?>

<body id="<?php echo $body_id ?>">

	<div id="outer">
		<div id="container">
			
			
		<?php include_once 'navigation.php'; ?>	
			
		<?php echo $slider; ?>	
			
			
			
			<div id="bd">

				<script type="text/javascript">
$(function(){
	switchImage();
	function switchImage() {
		var $active = $('.home-tile-large .title a.active');
		var $next = $active.next();
		while (!$next.data('image')) {
			$next = $next.next();
			if (!$next.length) {
				$next = $('.home-tile-large .title a:first');
			}
		}
		$active.removeClass('active');
		$next.addClass('active');
		$('.home-tile-large>a')
			.attr('href', $next.attr('href'))
			.off('click')
			.on('click', function(e){
				$next.trigger('click');
				if ($next.hasClass('lightbox')) {
					e.preventDefault();
				}
			})
			.find('IMG')
				.attr('src', $next.data('image'))
				.one('load', function(){
					setTimeout(switchImage, 6000);
				});
	}
});
</script>

				<div class="sidebar">
					<div class="content">

						<div class="section">
							<!--div id="sidebarsearch">
	<h2>Search</h2>
	<form action="/search/" method="get">
		<input type="text" name="q" id="SearchField">
		<input type="submit" class="submit" value="Submit">
	</form>
</div-->

<script>
  (function() {
    var cx = '009384887850083809149:biozs-wkhmm';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>

					</div>


			<?php echo $sidebar;?>

					</div>
				</div>

				<div class="main">
					
					<?php echo $maincontent ;?>
					
				</div>
				<div class="clearfix"></div>
			</div>


	<?php include_once 'footer.php';?>
