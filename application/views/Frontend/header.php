<!doctype html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
	<title> <?php echo $title; ?> | Nasrin Agriculture Industries Ltd.</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<!-- Document metadata -->
	<meta charset="utf-8"/>

	 

	<!-- Stylesheets -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>frontend/generated/css/screen.v1446775951.css" />
	<!-- Javascripts -->
	<?php 
	if($sliderjs){ ?>
	<script type="text/javascript" src="<?php echo base_url();?>frontend/generated/js/scripts.v1403675875.js"></script>
	<?php  } ?>
	<!-- Page scripts -->
	
	<!-- Google analytics -->
	<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-36022271-2']);
	_gaq.push(['_trackPageview']);
		(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
		</script>

</head>