
<div class="content content-with-pullout">
	<h1>About the New Hope Group</h1>
	<!--span class="subtitle"></span-->
	<div class="description">
		<p class="standout">The New Hope Group is an Australian owned and
			operated diversified energy company which has been proudly based in
			South East Queensland for more than 60 years.</p>
		<hr>
		<p class="pullout">
			Proudly<em> Australian owned</em> and <em>operated</em>
		</p>
		<p>
			With business interests and operations spanning coal mining,
			exploration, port operation, oil, agriculture, innovative
			technologies and investment, New Hope is ranked as one of the 100
			largest ASX listed companies by market capitalisation, and is amongst
			the top 10 ASX listed companies based in Queensland.<span>&nbsp;</span>
		</p>
		<p>The success of New Hope�s diversified ventures along with a
			reputation for hard work and sensible management has seen the
			business grow to become one of the State�s largest regionally based
			corporations, directly employing around 600 people.</p>
		<p>Our continued growth is founded on a long-term commitment to our
			employees and disciplined management, alongside a proactive approach
			to the environment, community and social responsibility.</p>
		<p>
			Outstanding business credentials and dedication to the communities in
			which we operate, has seen the company widely awarded, the latest
			recognition as <a
				href="../news/2012/new-hope-wins-ipswich-business-of-the-year-award.html">Ipswich
				Business of Year in 2012</a>.
		</p>
	</div>
</div>
