<?php

class Frontend extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');

		/*cache control*/
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
	
	}
	

	public function index()
	{
		$data = array();
	//	$data['notices'] = $this->frontend_model->noticeboard();
	//	$data['sliders'] = $this->frontend_model->sliders();
	//	$data['slider'] = 1;
	//	$data['left_sidebar'] = 1;
	//	$data['content'] = 1;
	//	$data['right_sidebar'] = 1;
		$data['title'] = 'Home';
		$data['body_id'] = 'section-default';
		$data['sliderjs'] = 1;
		$data['sliderpic'] = 'all';
		$data['slider'] = $this->load->view('frontend/slider', $data, true);
		$data['sidebar'] = $this->load->view('frontend/sidebar', '', true);
		$data['maincontent'] = $this->load->view('frontend/maincontent', '', true);
		$this->load->view('frontend/home', $data);
	}

	public function about()
	{
		$data = array();
	//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
	//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
	//	$data['notices'] = $this->frontend_model->noticeboard();
	//	$data['sliders'] = $this->frontend_model->sliders();
	//	$data['slider'] = '';
	//	$data['left_sidebar'] = '';
	//	$data['content'] = 1;
	//	$data['bottom'] = '';
	//	$data['style'] =1;
		$data['nav_active'] = 'about';
		$data['title'] = 'About';
		$data['body_id'] = 'section-about';
		$data['sliderjs'] = "";
		$data['sliderpic'] = 'about';
		$data['slider'] = $this->load->view('frontend/slider', $data, true);
		$data['sidebar'] = $this->load->view('frontend/about_menu', $data, true);
		$data['maincontent'] = $this->load->view('frontend/about', '', true);
		$this->load->view('frontend/home', $data);
	}
	
	public function management()
	{
		$data = array();
		$data['title'] = 'Management';
		$data['body_id'] = 'section-about';
		$data['sliderjs'] = "";
		$data['sliderpic'] = 'management';
		$data['nav_active'] = 'management';
		$data['slider'] = $this->load->view('frontend/slider', $data, true);
		$data['sidebar'] = $this->load->view('frontend/about_menu', $data, true);
		$data['maincontent'] = $this->load->view('frontend/leadership', '', true);
		$this->load->view('frontend/home', $data);
	}

	public function MissionVission()
	{
		$data = array();
		$data['title'] = 'Mission Vission';
		$data['body_id'] = 'section-about';
		$data['sliderjs'] = "";
		$data['sliderpic'] = 'mission_vission';
		$data['nav_active'] = 'mission_vission';
		$data['slider'] = $this->load->view('frontend/slider', $data, true);
		$data['sidebar'] = $this->load->view('frontend/about_menu', $data, true);
		$data['maincontent'] = $this->load->view('frontend/mission_vission', '', true);
		$this->load->view('frontend/home', $data);
	}
	
	
	public function environment()
	{
		$data = array();
		$data['title'] = 'Environment';
		$data['body_id'] = 'section-about';
		$data['sliderjs'] = "";
		$data['sliderpic'] = 'environment';
		$data['nav_active'] = 'environment';
		$data['slider'] = $this->load->view('frontend/slider', $data, true);
		$data['sidebar'] = $this->load->view('frontend/about_menu', $data, true);
		$data['maincontent'] = $this->load->view('frontend/environment', '', true);
		$this->load->view('frontend/home', $data);
	}
	
	
	public function history()
	{
		$data = array();
		$data['title'] = 'Our History';
		$data['body_id'] = 'section-about';
		$data['sliderjs'] = "";
		$data['sliderpic'] = 'history';
		$data['nav_active'] = 'history';
		$data['slider'] = $this->load->view('frontend/slider', $data, true);
		$data['sidebar'] = $this->load->view('frontend/about_menu', $data, true);
		$data['maincontent'] = $this->load->view('frontend/history', '', true);
		$this->load->view('frontend/home', $data);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function intro()
	{
	
		$data = array();
		$data['notices'] = $this->frontend_model->noticeboard();
		$data['sliders'] = $this->frontend_model->sliders();
		
		$data['slider'] = 1;
		$data['left_sidebar'] = 1;
		$data['content'] = 1;
		$data['right_sidebar'] = 1;
		$data['bottom'] = 1;
		$data['maincontent'] = $this->load->view('frontend/about_us/intro', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function m_committe()
	{
		$data = array();
	
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/about_us/m_committe', '', true);
		$data['title'] = 'info';//for datatable
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function mission_vission()
	{
		$data = array();
		$data['notices'] = $this->frontend_model->noticeboard();
		$data['sliders'] = $this->frontend_model->sliders();
		
		$data['slider'] = 1;
		$data['left_sidebar'] = 1;
		$data['content'] = 1;
		$data['right_sidebar'] = 1;
		$data['bottom'] = 1;
		$data['maincontent'] = $this->load->view('frontend/about_us/mission_vission', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function infrastructure()
	{
		$data = array();
		$data['notices'] = $this->frontend_model->noticeboard();
		$data['sliders'] = $this->frontend_model->sliders();
		
		$data['slider'] = 1;
		$data['left_sidebar'] = 1;
		$data['content'] = 1;
		$data['right_sidebar'] = 1;
		$data['bottom'] = 1;
		$data['maincontent'] = $this->load->view('frontend/about_us/infrastructure', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function guardian_panel()
	{
		$data = array();
		$data['notices'] = $this->frontend_model->noticeboard();
		$data['sliders'] = $this->frontend_model->sliders();
		
		$data['slider'] = 1;
		$data['left_sidebar'] = 1;
		$data['content'] = 1;
		$data['right_sidebar'] = 1;
		$data['bottom'] = 1;
		$data['maincontent'] = $this->load->view('frontend/about_us/guardian_panel', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function founders()
	{
		$data = array();
		$data['notices'] = $this->frontend_model->noticeboard();
		$data['sliders'] = $this->frontend_model->sliders();
		
		$data['slider'] = 1;
		$data['left_sidebar'] = 1;
		$data['content'] = 1;
		$data['right_sidebar'] = 1;
		$data['bottom'] = 1;
		$data['maincontent'] = $this->load->view('frontend/about_us/founders', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
		
	public function achievements()
	{
		$data = array();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style']=1;
		$data['maincontent'] = $this->load->view('frontend/about_us/achievements', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	
	public function donors()
	{
		$data = array();
		$data['notices'] = $this->frontend_model->noticeboard();
		$data['sliders'] = $this->frontend_model->sliders();
		
		$data['slider'] = 1;
		$data['left_sidebar'] = 1;
		$data['content'] = 1;
		$data['right_sidebar'] = 1;
		$data['bottom'] = 1;
		$data['maincontent'] = $this->load->view('frontend/about_us/donors', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
	
		$this->load->view('frontend/home', $data);
	}
	
	public function principal_of_the_institute()
	{
		$data = array();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/teachers_info/principal_of_the_institute', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function former_head_of_the_institute()
	{
		$data = array();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/teachers_info/former_head_of_the_institute', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function head_of_the_institute()
	{
		$data = array();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/teachers_info/head_of_the_institute', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	
	
	
		
	
	public function all_teachers()
	{
		$data = array();
		//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/teachers_info/all_teachers', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
		
	public function all_stuffs()
	{
		$data = array();
		//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/teachers_info/all_stuffs', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}	
	
	
	public function students_info()
	{
		$this->load->model('frontend_model');
		$data = array();
		$class_id = $this->input->post("class_id");
		$section_id = $this->input->post("section_id");
		$data['students_by_class_and_sections'] = $this->frontend_model->students_by_class_and_sections($class_id, $section_id);
		$data['all_class'] = $this->frontend_model->all_class();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/students_info', $data, true);
		$data['title'] = 'info';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}	
	
	public function student_attendance_by_class()
	{
		$this->load->model('frontend_model');
		$data = array();
		$data['first_date'] = date('Y-m-d', strtotime($this->input->post("first_date"))); 
		$data['last_date'] = date('Y-m-d', strtotime($this->input->post("last_date"))); 
		$first_date = date('Y-m-d', strtotime($this->input->post("first_date"))); 
		$last_date = date('Y-m-d', strtotime($this->input->post("last_date"))); 
		$class_id = $this->input->post("class_id");
		$section_id = $this->input->post("section_id");
		$data['class_id'] = $this->input->post("class_id");
		$data['section_id'] = $this->input->post("section_id");
		$data['students_by_class_section'] = $this->frontend_model->students_by_class_section($class_id, $section_id, $first_date, $last_date);
	//	$data['total_present_students'] = $this->frontend_model->total_present_students($class_id, $section_id, $first_date, $last_date);
		$data['all_class'] = $this->frontend_model->all_class();
		$data['inputbox'] = $this->input->post('inputbox');
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['navstyle'] = 1;
		$data['maincontent'] = $this->load->view('frontend/student_attendance_by_class', $data, true);
		$data['title'] = 'info';
		$data['boot'] = 'admit';
		
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function student_attendance_by_date()
	{
		$this->load->model('frontend_model');
		$data = array();
		$data['first_date'] = date('Y-m-d', strtotime($this->input->post("first_date"))); 
		$data['last_date'] = date('Y-m-d', strtotime($this->input->post("last_date"))); 
		$first_date = date('Y-m-d', strtotime($this->input->post("first_date"))); 
		$last_date = date('Y-m-d', strtotime($this->input->post("last_date"))); 
		$data['classes_by_date'] = $this->frontend_model->classes_by_date($first_date, $last_date);
		$data['all_class'] = $this->frontend_model->all_class();
		$data['inputbox'] = $this->input->post('inputbox');
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['navstyle'] = 1;
		$data['maincontent'] = $this->load->view('frontend/student_attendance_by_date', $data, true);
		$data['title'] = 'info';
		$data['boot'] = 'admit';
		
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}

	
	
	public function ajax_class($class_id)
	{
		$this->load->model('frontend_model');
		
		$data = $this->frontend_model->class_sections($class_id);
		
		echo $data;
	}
	
	public function ajax_subjects($class_id, $section_id)
	{
		$query = $this->db->get_where('subject', array('class_id' => $class_id, 'section_id'=> $section_id, 'priority'=> 1));
		$output = null;
		
		foreach ($query->result() as $row)
		{
			$output .= "<div class='col-sm-4'> <input type='hidden' name ='subject_id[]' value='".$row->subject_id."'><span class='form-control' id='disabledInput' disabled>".$row->name."</span><br></div>";
		}
		
		
		
		
		echo $output;
	}
		
	
	public function ajax_opt_subjects($class_id, $section_id)
	{
		
	 	$query = $this->db->get_where('subject', array('class_id' => $class_id, 'section_id'=> $section_id, 'priority'=> 0));
		$output = null;
		
		foreach ($query->result() as $row)
		{
			$output .= "<option value='".$row->subject_id."'>".$row->name."</option>";
		}
		
	
		echo $output;
		
	}
	
	
	
	
	
	public function student_admit()
	{
		$this->load->model('frontend_model');
		$data = array();
		$data['all_class'] = $this->frontend_model->all_class();
		//$data['all_class'] = $this->frontend_model->all_class();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
	//	$data['subject_id'] = $this->frontend_model->all_subject_id()
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['navstyle']=1;
		$data['maincontent'] = $this->load->view('frontend/student_admit', $data, true);
		$data['title'] = 'admit';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function admission_apply()
	{
		$fdata['name']  = $this->input->post('father_name');
		$fdata['email']  = 'parent@gmail.com';
		$fdata['password']  = '1234';
		$fdata['phone']  = $this->input->post('father_mobile_no');
		$fdata['address']  = $this->input->post('present_house_name');
		$fdata['profession']  = $this->input->post('father_occupation');
		
			
		$this->db->insert('parent', $fdata);
		$parent_id = $this->db->insert_id();
		
		$data['name']       				= $this->input->post('name');
		$data['bangla_name']       			= $this->input->post('bangla_name');
		$data['birthday']       			= date('Y-m-d', strtotime($this->input->post('birthday')));
		$data['sex']      					= $this->input->post('sex');
		$data['religion']      	 			= $this->input->post('religion');
		$data['nationality']      			= $this->input->post('nationality');
		$data['phone']       				= $this->input->post('phone');
		$data['email']       				= $this->input->post('email');
		$data['password']       			= $this->input->post('password');
		$data['class_id']       			= $this->input->post('class_id');
		$data['section_id']       			= $this->input->post('section_id');
		$data['subject_id']       			= serialize($this->input->post('subject_id'));
	
		if(!empty($this->input->post('rec_sub')) && !empty($this->input->post('opt_sub'))){
		$data['rec_sub']       				= $this->input->post('rec_sub');
		$data['opt_sub']       				= $this->input->post('opt_sub');
		}
		
		$data['prev_school_name']   		= $this->input->post('prev_school_name');
		$data['father_name']       			= $this->input->post('father_name');
		$data['father_bangla_name'] 		= $this->input->post('father_bangla_name');
		$data['father_occupation']  		= $this->input->post('father_occupation');
		$data['father_mobile_no']   		= $this->input->post('father_mobile_no');
		$data['father_nid']       			= $this->input->post('father_nid');
		$data['mother_name']       			= $this->input->post('mother_name');
		$data['mother_bangla_name'] 		= $this->input->post('mother_bangla_name');
		$data['mother_nid']       			= $this->input->post('mother_nid');
		$data['mother_mobile_no']  	 		= $this->input->post('mother_mobile_no');
		$data['legal_guardian_name']       	= $this->input->post('legal_guardian_name');
		$data['legal_guardian_bangla_name'] = $this->input->post('legal_guardian_bangla_name');
		$data['legal_guardian_occupation']  = $this->input->post('legal_guardian_occupation');
		$data['legal_guardian_mobile_no']   = $this->input->post('legal_guardian_mobile_no');
		$data['legal_guardian_relation']    = $this->input->post('legal_guardian_relation');
		$data['present_house_name']       	= $this->input->post('present_house_name');
		$data['present_word_no']      		= $this->input->post('present_word_no');
		$data['present_village']       		= $this->input->post('present_village');
		$data['present_union']       		= $this->input->post('present_union');
		$data['present_post_office']       	= $this->input->post('present_post_office');
		$data['present_thana']       		= $this->input->post('present_thana');
		$data['present_district']       	= $this->input->post('present_district');
		$data['permanent_house_name']       = $this->input->post('permanent_house_name');
		$data['permanent_word_no']       	= $this->input->post('permanent_word_no');
		$data['permanent_village']       	= $this->input->post('permanent_village');
		$data['permanent_union']       		= $this->input->post('permanent_union');
		$data['permanent_post_office']    	= $this->input->post('permanent_post_office');
		$data['permanent_thana']       		= $this->input->post('permanent_thana');
		$data['permanent_district']       	= $this->input->post('permanent_district');
		$data['publication_status']       	= $this->input->post('publication_status');
		
		$data['parent_id']  = $parent_id;

		$this->db->insert('student', $data);
		$student_id = $this->db->insert_id();
		move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/student_image/' . $student_id . '.jpg');
		$this->session->set_flashdata('flash_message' ,'আপনার আবেদনটি সফলভাবে গ্রহণ করা হয়েছে। পরবর্তী প্রক্রিয়ার জন্য স্কুলের সাথে যোগাযোগ করুন।');
		//$this->email_model->account_opening_email('student', $data['email']); //SEND EMAIL ACCOUNT OPENING EMAIL

		redirect(base_url() . 'frontend/student_admit', 'refresh');
		
		/**
		$this->load->library('Pdf');
		
		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
		$pdf->SetTitle('My Title');
		$pdf->SetHeaderMargin(30);
		$pdf->SetTopMargin(20);
		$pdf->setFooterMargin(20);
		$pdf->SetAutoPageBreak(true);
		$pdf->SetAuthor('Author');
		$pdf->SetDisplayMode('real', 'default');
		

		$pdf->AddPage();
		
		$pdf->SetCreator(PDF_CREATOR);
		// create address box
		
		// $pdf->Write(5, 'kuttar chalit');
		
		
		$pdf->Write(5, 'Sasdfasdfasdfext');
		
		
		$pdf->Output('My-File-Name.pdf','I');
		**/
		
		
		
	}
	
	
	
	public function noticeboard()
	{
		$this->load->model('frontend_model');
		$data = array();
		$data['notices'] = $this->frontend_model->noticeboard();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/noticeboard', $data, true);
		$data['title'] = 'info';//bootstrap loading er issue ace
		$this->load->view('frontend/home', $data);
	}
	
	public function notice_single($notice_id = false )
	{
		$this->load->model('frontend_model');
		$data = array();
		$data['notice_single'] = $this->frontend_model->notice_single($notice_id);
		//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/notice_single', $data, true);
		$data['title'] = 'Notice';
		$this->load->view('frontend/home', $data);
	}
	
	public function news()
	{
		$this->load->model('frontend_model');
		$data = array();
		$data['news'] = $this->frontend_model->news();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/news', $data, true);
		$data['title'] = 'info';//bootstrap loading er issue ace
		$this->load->view('frontend/home', $data);
	}
	
	public function news_single($news_id = false )
	{
		$this->load->model('frontend_model');
		$data = array();
		$data['news_single'] = $this->frontend_model->news_single($news_id);
		//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/news_single', $data, true);
		$data['title'] = 'Notice';
		$this->load->view('frontend/home', $data);
	}	
	
	public function gallery()
	{
		$this->load->model('frontend_model');
		$data = array();
		
		$data['gallery'] = $this->frontend_model->gallery();
		
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['navstyle']=1;
		$data['maincontent'] = $this->load->view('frontend/gallery', $data, true);
		$data['title'] = 'admit';
		$this->load->view('frontend/home', $data);
	}
	
	public function important_web_links()
	{
		$this->load->model('frontend_model');
		$data = array();
		//$data['notices'] = $this->frontend_model->noticeboard();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/important_web_links', $data, true);
		$data['title'] = 'info';//bootstrap loading er issue ace
		$this->load->view('frontend/home', $data);
	}
	
	public function internal_exam_result()
	{
		$this->load->model('frontend_model');
		$data = array();
		$date = $this->input->post('date');
		$exam_id = $this->input->post('exam_id');
		$student_id = $this->input->post('student_id');
		$data['date'] = $this->input->post('date');
		$data['student_id'] = $this->input->post('student_id');
		$data['exam_id'] = $this->input->post('exam_id');
		$data['student_info'] = $this->db->get_where('student', array('student_id'=>$student_id))->row_array(); 
		$data['internal_exam_result'] = $this->frontend_model->internal_exam_result($date, $exam_id, $student_id);
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['navstyle'] = 1;
		$data['maincontent'] = $this->load->view('frontend/internal_exam_result', $data, true);
		$data['title'] = 'Home';
		$data['title'] = 'info';
		$data['boot'] = 'admit';
		$this->load->view('frontend/home', $data);
	}
	
	public function ajax_exams($date)
	{
		$query = $this->db->get_where('exam', array('date' => $date));
		
		$output = null;
		
		foreach ($query->result() as $row)
		{
			$output .= "<option value='".$row->exam_id."'>".$row->full_name."</option>";
		}
		
		echo $output;
	}	
	
	
	public function contact()
	{
		$data = array();
		//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/contact', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	
	public function events()
	{
		$data = array();
		//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/events', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function class_routine()
	{
		$data = array();
		//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/class_routine', '', true);
		$data['title'] = 'admit';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function syllabus()
	{
		$data = array();
		//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/syllabus', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function library()
	{
		$data = array();
		//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/library', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function admission_info()
	{
		$data = array();
		//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/admission_info', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function academic_calendar()
	{
		$data = array();
		//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/academic_calendar', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function rules_and_regulations()
	{
		$data = array();
		//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/rules_and_regulations', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function direction()
	{
		$data = array();
		//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/direction', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function cultural_function()
	{
		$data = array();
		//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/cultural_function', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function multimedia_classroom()
	{
		$data = array();
		//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/multimedia_classroom', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function sports()
	{
		$data = array();
		//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/sports', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function scouts()
	{
		$data = array();
		//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/scouts', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function study_tour()
	{
		$data = array();
		//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/study_tour', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function science_lab()
	{
		$data = array();
		//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/science_lab', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function computer_lab()
	{
		$data = array();
		//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/computer_lab', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function ebook()
	{
		$data = array();
		//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/ebook', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function digital_content()
	{
		$data = array();
		//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/digital_content', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}
	
	public function download()
	{
		$data = array();
		//	$data['all_published_category'] = $this->welcome_model->select_all_published_category();
		//	$data['all_published_blog'] = $this->welcome_model->select_all_published_blog();
		$data['slider'] = '';
		$data['left_sidebar'] = '';
		$data['content'] = 1;
		$data['right_sidebar'] = '';
		$data['bottom'] = '';
		$data['style'] = 1;
		$data['maincontent'] = $this->load->view('frontend/download', '', true);
		$data['title'] = 'Home';
		//	$data['slider'] = 1;
		$this->load->view('frontend/home', $data);
	}

	
	
	
	
}